# Copyright (c) 2009 Markus Rothe
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.17 ] ]

SUMMARY="File transfer client supporting a number of network protocols"
DESCRIPTION="
LFTP is sophisticated ftp/http client, file transfer program supporting a number of network
protocols. Like BASH, it has job control and uses readline library for input. It has bookmarks,
built-in mirror, can transfer several files in parallel. It was designed with reliability in mind.
"
HOMEPAGE="https://lftp.yar.ru/"
DOWNLOADS="https://lftp.yar.ru/ftp/${PNV}.tar.xz"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    debug
    idn
    ( linguas: cs de es fr it ja ko pl pt_BR ru uk zh_CN zh_HK zh_TW )
    ( providers: gnutls libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext
    build+run:
        dev-libs/expat
        sys-libs/ncurses
        sys-libs/readline:=
        idn? ( net-dns/libidn2 )
        providers:gnutls? ( dev-libs/gnutls )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
"

# tests connect to various servers on the internet
RESTRICT="test"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/Fix-build-with-LibreSSL-following-commit-537f37898.patch
    "${FILES}"/${PNV}-Fix-C99-compatibility-issue.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
    --enable-packager-mode
    --with-modules
    gt_cv_func_gnugettext{1,2}_libc=yes

)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    debug
    '!providers:gnutls openssl'
    'providers:gnutls gnutls'
    'idn libidn2'
)

